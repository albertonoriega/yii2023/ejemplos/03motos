<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "motos".
 *
 * @property int $id
 * @property string|null $matricula
 * @property float|null $precio
 * @property string|null $marca
 * @property string|null $modelo
 */
class Motos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'motos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['precio'], 'number'],
            [['matricula'], 'string', 'max' => 15],
            [['marca', 'modelo'], 'string', 'max' => 100],
            [['matricula'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'matricula' => 'Matricula',
            'precio' => 'Precio',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
        ];
    }
}
